##
# Hello hello!
#
# This is meant to be a quick (and fun?) sense-check of your Ruby!
# 
# Notes:
#  1. Feel free to download the CSV locally paste into irb or run this with `ruby requests.rb`!
#  2. We're loosely available to field questions on recruiting+fullstack@trychameleon.com
#  3. Don't overanalyze your implementation/formatting/variable names/etc. do what comes naturally to you
#  4. More than anything, thanks for taking the time to work through this! -BN
#
require 'uri'
require 'net/http'
require 'csv'
require 'time'

uri = URI('https://chmln-east.s3.amazonaws.com/tmp/recruiting/fullstack/requests.csv'); nil
res = Net::HTTP.get_response(uri); nil
csv = CSV.parse(res.body); nil

csv.shift
#=> ["request id","timestamp","user id","method","path","response time","db time"]
csv.sample
#=> ["864197", "2028-03-23T02:44:49.192Z", "5fb3392954558f806fe97441", "PATCH", "/profiles/:id", "20ms", "7ms"]


##
# 1. average response time
puts '1'
# => 

##
# 2. number of users online
puts '2'
# => 

##
# 3. ids of the 5 slowest requests
puts '3'
# => 

##
# Bonus question: Imagine you received this CSV as a "stream" and had, at any point in parsing the file, to
#  output the "current top 5 slowest requests".
# 
# 4. What is the state of your data structure(s) at row 75121.
puts '4'
#=> 